
Page({
  data: {
    finishedQuantity:'',
    workSheetId:''},
  onSubmit(e) {
    dd.showLoading();
    // dd.alert({
    //   content: `数据：${JSON.stringify(e.detail.value)}`,
    // });
    console.log( JSON.stringify(e.detail.value));
      // 1. 提交报工数据
    let domain = "http://lxmes.vaiwan.com";
    let url = domain + '/mes/dingTalk/addOperationReport';
    dd.httpRequest({
        url: url,
        method: 'POST',
        headers: {
        "Content-Type": "application/json",
        "X-Access-Tenant":"99070603008409600",
        },
        data: JSON.stringify(e.detail.value)
        ,
        dataType: 'json',
        success: (res) => {
            if (res && res.data.success) {
                console.log('httpRequest success --->', res);
                dd.showToast({
                    content: '报工成功',
                    type: 'success',
                    duration: 1200,
                    success() {
                      console.log('toast end');
                    // this.navigateBack()
                    dd.navigateBack()
                  },
              });

            } else {
                console.log("httpRequest failed --->", res);
                // dd.alert({content: JSON.stringify(res)});
            }
        },
        fail: (res) => {
            console.log("httpRequest failed --->", res);
            // dd.alert({content: JSON.stringify(res)});
        },
        complete: (res) => {
            dd.hideLoading();
        }
        
    });

    
  },
  onReset() {
      this.setData({
      finishedQuantity:'',
      workSheetId:''
  });
},

});
